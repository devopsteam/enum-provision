# ENUM Provisioning

## Intro

This is a small PoC in Python to show how to provision ENUM records (NAPTR, SRV, A) in DNS.
In this case using the API provided by PowerDNS for the provisioning interface.

There are two main types of 'things' to deal with:

 * Numbers ranges: these are assigned in full to 1 Operator
 * Ported Numbers: these are individual numbers which are ported to a different Operator than the Number Range Operator.

## Number Ranges

Number ranges are defined by a start number and end number, all numbers in sequence from the start number until the end number are part of the range.
In NL, mobile numbers starting with 316 have a fixed length of 11 digits.

### Example:

Consider the range defined by:

 - start: 31624090000
 - end:   31624099999

 This has a prefix of: 3162409
 All numbers from 3162409.0000 until 3162409.9999 are in this range, and point to 1 Operator.

 In DNS this is indicated by the NAPTR record for the ENUM domain. The domain can be setup to correspond to the prefix.

 - prefix: 3162409
 - ENUM domain name: ```9.0.4.2.6.1.3.e164.arpa.```

 The NAPTR records for this domain are valid for all the numbers in the Number Range, and point to the assigned Operator.


## Ported Numbers

 Ported Numbers must override the default NAPTR records and point to the new Operator. This can be achieved in DNS by simply creating a new zone for that number and setting the appropriate NAPTR records.

### Example:

 The number 31624096744 falls in the Number Range defined above, but is Ported to a different Operator. In order to override the settings we simply configure the overriding domain.

  - ported number: 31624096744
  - ENUM domain name: ```4.4.7.6.9.0.4.2.6.1.3.e164.arpa.```

  In this domain we set the NAPTR records for the new Operator.

# Using the provisioning scripts

## Files

The following files are used:

- ```provision-domains.py```: provisions the Operator zones based on an input file
- ```provision-ranges.py```:  provisions the ranges as assigned to each Operator
- ```provision-m2mranges.py```: provisions the M2M ranges as assigned to each Operator
- ```provision-ported-numbers.py```: provisions all numbers that have been ported to a different Operator than the original range

Configuration files are found in the etc/ subdir. You can configure the DNS server's API URL and secret key.

## Syntax
To execute the commands simply type:

```provision-xyz.py inputfile```

```provision-xyz.py -h``` provides simple help text

```provision-xyz.py -n inputfile``` simulates the run without actually updating the DNS


```dns-client.py number``` goes through the ENUM flow step by step with verbose output for the specified number