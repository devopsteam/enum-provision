import requests
import json

def createzone(payload, opts):
    r = requests.post(opts['apiurl'], data=json.dumps(payload), headers=opts['headers'])
    return r.text;

def updaterecords(payload, opts, zone):
    # don't forget the extra "/" or it will not work!
    r = requests.patch(opts['apiurl']+"/"+zone, data=json.dumps(payload), headers=opts['headers'])
    return r.text;

def getnaptr(opcode):
    opdomain = {
        'ASPV': 'aspider.nl',
        'ATTM': 'att.com',
        'BELM': 'belgacom.be',
        'BEN': 'ben.nl',
        'BLYK': 'blyk.nl',
        'BMNL': 'bmnl.nl',
        'COMB': 'comb.nl',
        'DCMO': 'dcmo.nl',
        'DUTO': 't-mobile.nl',
        'ETMB': 'etmb.nl',
        'GSM1': 'kpn.nl',
        'ICMC': 'icmc.nl',
        'INMO': 'inmo.nl',
        'KEEM': 'keem.nl',
        'LTEL': 'libertel.nl',
        'LYCA': 'lycatel.nl',
        'MOMO': 'momo.nl',
        'PMNL': 'pmnl.nl',
        'PRMB': 'prmb.nl',
        'SCAR': 'scarlet.nl',
        'TEL2': 'tele2.nl',
        'TLFM': 'telefonica.com',
        'TLNM': 'telenor.com',
        'UNIF': 'unif.nl',
        'UPCM': 'upc.com',
        'VWMO': 'vwmo.nl',
        'ZIMO': 'ziggo.nl'
    }
    try:
        domain = opdomain[opcode]
    except:
        print "Error: Don't know domain {0}, please check!\n".format(opcode)
        domain = "UNKNOWN.NL"

    _naptr = "100 10 \"U\" \"E2U+sip\" \"!^(.*)$!sip:\\\\1@{0}!\" ."
    naptr = _naptr.format(domain)

    return naptr;
