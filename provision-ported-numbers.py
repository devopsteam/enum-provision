#!/usr/bin/python2.7

###
# process command line options
# -h for help
# -n for no-op, only simulate the run
import sys, getopt
argv = sys.argv[1:]
# set some default
fakeit = False
helptext = sys.argv[0]+' [-hn] inputfile\n\th: help\n\tn: no action. simulate only...\n'
try:
    opts, args = getopt.getopt(argv,"hn")
    # we expect 1 input file name
    if not args:
        print "You must specify an input file\n", helptext
        sys.exit(2)
    else:
        filename = args[0]
except getopt.GetoptError:
    print "Error: Invalid options.\n", helptext
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print helptext
        sys.exit()
    elif opt == "-n":
        fakeit = True
###

from etc.secrets import getconfig
from etc.dns     import *

config = getconfig()

opts = {}
opts['apiurl']  = config['apiurl']
opts['headers'] = config['headers']

with open(filename, "r") as infile:
    for line in infile:
        nums = line.split(",")

        name = ".".join(list(nums[0][::-1]))+".1.3.e164.arpa."

        # get the naptr based on the operator code in pos 1
        naptr = getnaptr(nums[1])

        # create the zone
        payload1 = {
            "account": "provisioning",
            "soa_edit_api": "INCEPTION-INCREMENT",
            "name": name,
            "kind": "Native",
            "nameservers": ["ns1.guizy.co.", "ns2.guizy.co."]
            }

        if fakeit:
            print payload1
        else:
            createzone(payload1, opts)

        # add the NAPTR records
        payload2 = {
            "kind": "Native",
            "name": name,
            "rrsets": [
            {
            "name": name,
            "changetype": "REPLACE",
            "kind": "Native",
            "type": "NAPTR",
            "ttl": 3600,
            "records": [{"content": naptr, "disabled": False}]
            }
        ] }
        if fakeit:
            print payload2
        else:
            updaterecords(payload2, opts, name)
