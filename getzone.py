#!/usr/bin/python2.7

###
# process command line options
# -h for help
# -n for no-op, only simulate the run
import sys, getopt
argv = sys.argv[1:]

# set some defaults
fakeit = False
helptext = sys.argv[0]+' -[h] domain | jq \n\th: help\n'
try:
    opts, args = getopt.getopt(argv,"h")
    # we expect 1 input file name
    if not args:
        print "You must specify a domain name\n", helptext
        sys.exit(2)
    else:
        zone = args[0]
except getopt.GetoptError:
    print helptext
    sys.exit(2)
for opt, arg in opts:

    if opt == '-h':
        print helptext
        sys.exit()
###


from etc.secrets import getconfig
from etc.dns     import *

config = getconfig()

opts = {}
opts['apiurl']  = config['apiurl']
opts['headers'] = config['headers']

print getzone(opts, zone)

