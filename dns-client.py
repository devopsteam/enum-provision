#!/usr/bin/python2.7
import re

###
# process command line options
# -h for help
# -n for no-op, only simulate the run
import sys, getopt
argv = sys.argv[1:]
# set some default
fakeit = False
helptext = sys.argv[0]+' [-h] number (e.g. 31624095000)\n\th: help\n\tThis script will do the dns lookups simulating a sip client\n'
try:
    opts, args = getopt.getopt(argv,"hn")
    # we expect 1 input file name
    if not args:
        print "You must provide a number (e.g. mobile 31624095000)\n", helptext
        sys.exit(2)
    else:
        number = args[0]
except getopt.GetoptError:
    print "Error: Invalid options.\n", helptext
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print helptext
        sys.exit()

###

print "Here we go, using: ", number

# allow 6xxx format
number = re.sub("^6", "316", number)

print "\nStep 1: convert to e164 format...\n"
import dns.e164
n = dns.e164.from_e164(number)
print(n)
nr = dns.e164.to_e164(n)
print(nr)

print "\nStep 2: find the NAPTR record for this e164.arpa domain...\n"
import dns.resolver
mydns = dns.resolver.Resolver()
mydns.nameservers=['172.17.0.3']

print "NAPTR breakdown:"
answers = mydns.query(n, 'NAPTR')
for rdata in answers:
    print "NAPTR ", rdata
    print "Order:\t\t", rdata.order
    print "Preference:\t", rdata.preference
    print "Flags:\t\t", rdata.flags
    print "Service:\t", rdata.service
    print "Regexp:\t\t", rdata.regexp
    print "Replacement:\t", rdata.replacement

regex = rdata.regexp

print "\nStep 3: Apply Regexp to {0}\n".format(nr)
r = regex.split("!")
l = re.sub(r[1],r[2],  nr)
print "Regexp, got ", l

domain = l.split("@", 1)[1]
print "Operator domain: ", domain

print "\nStep 4: Find the NAPTR record for the domain {0}\n".format(domain)
print "NAPTR breakdown:"
answers = mydns.query(domain, 'NAPTR')
for rdata in answers:
    print "NAPTR ", rdata
    print "Order:\t\t", rdata.order
    print "Preference:\t", rdata.preference
    print "Flags:\t\t", rdata.flags
    print "Service:\t", rdata.service
    print "Regexp:\t\t", rdata.regexp
    print "Replacement:\t", rdata.replacement
service = rdata.service


print "\nStep 5: Find the SRV record for {0}\n".format(rdata.replacement)
print "SRV breakdown:"
answers = mydns.query(rdata.replacement, 'SRV')
for rdata in answers:
    print "SRV ", rdata
    print "Priority:\t", rdata.priority
    print "Weight:\t\t", rdata.weight
    print "Port:\t\t", rdata.port
    print "Target:\t\t", rdata.target
port = rdata.port

print "\nStep 6: Find the A record for {0}\n".format(rdata.target)
print "A record (IP address):"
answers = mydns.query(rdata.target, 'A')
for rdata in answers:
    print "A ", rdata
address = rdata


print "\nStep 7: Put it all together\n"
print "Client can now create a call to:\n"
print "{0} {1}@{2}:{3}".format(service, nr, address, port)
