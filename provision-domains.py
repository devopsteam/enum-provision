#!/usr/bin/python2.7

from etc.secrets import getconfig
from etc.dns     import *

config = getconfig()

opts = {}
opts['apiurl']  = config['apiurl']
opts['headers'] = config['headers']

# opcodes
operator = {
    'ASPV': {'domain': 'aspider.nl', 'gateway': 'sbc1.aspider.nl', 'addr': '10.41.123.12'},
    'ATTM': {'domain': 'att.com', 'gateway': 'sbc1.att.com', 'addr': '192.168.224.2'},
    'BELM': {'domain': 'belgacom.be', 'gateway': 'sbc1.belgacom.be', 'addr': '10.123.12.1'},
    'BEN':  {'domain': 'ben.nl', 'gateway': 'sbc.ben.nl', 'addr': '10.241.234.12'},
    'BLYK': {'domain': 'blyk.nl', 'gateway': 'sbc1.blyk.nl', 'addr': '10.21.24.120'},
    'BMNL': {'domain': 'bmnl.nl', 'gateway': 'sip.bmnl.nl', 'addr': '10.140.23.2'},
    'COMB': {'domain': 'comb.nl', 'gateway': 'sbc.comb.nl', 'addr': '10.241.234.12'},
    'DCMO': {'domain': 'dcmo.nl', 'gateway': 'sbc.dcmo.nl', 'addr': '10.241.234.12'},
    'DUTO': {'domain': 't-mobile.nl', 'gateway': 'sbc.t-mobile.nl', 'addr': '10.241.234.12'},
    'ETMB': {'domain': 'etmb.nl', 'gateway': 'sbc.etmb.nl', 'addr': '10.241.234.12'},
    'GSM1': {'domain': 'kpn.nl', 'gateway': 'sbc.kpn.nl', 'addr': '10.241.234.12'},
    'ICMC': {'domain': 'icmc.nl', 'gateway': 'sbc.icmc.nl', 'addr': '10.241.234.12'},
    'INMO': {'domain': 'inmo.nl', 'gateway': 'sbc.inmo.nl', 'addr': '10.241.234.12'},
    'KEEM': {'domain': 'keem.nl', 'gateway': 'sbc.keem.nl', 'addr': '10.241.234.12'},
    'LTEL': {'domain': 'libertel.nl', 'gateway': 'sbc.libertel.nl', 'addr': '10.241.234.12'},
    'LYCA': {'domain': 'lycatel.nl', 'gateway': 'sbc.lycatel.nl', 'addr': '10.241.234.12'},
    'MOMO': {'domain': 'momo.nl', 'gateway': 'sbc.momo.nl', 'addr': '10.241.234.12'},
    'PMNL': {'domain': 'pmnl.nl', 'gateway': 'sbc.pmnl.nl', 'addr': '10.241.234.12'},
    'PRMB': {'domain': 'prmb.nl', 'gateway': 'sbc.prmb.nl', 'addr': '10.241.234.12'},
    'SCAR': {'domain': 'scarlet.nl', 'gateway': 'sbc.scarlet.nl', 'addr': '10.241.234.12'},
    'TEL2': {'domain': 'tele2.nl', 'gateway': 'sbc.tele2.nl', 'addr': '10.241.234.12'},
    'TLFM': {'domain': 'telefonica.com', 'gateway': 'sbc.telefonica.com', 'addr': '10.241.234.12'},
    'TLNM': {'domain': 'telenor.com', 'gateway': 'sbc.telenor.com', 'addr': '10.241.234.12'},
    'UNIF': {'domain': 'unif.nl', 'gateway': 'sbc.unif.nl', 'addr': '10.241.234.12'},
    'UPCM': {'domain': 'upc.com', 'gateway': 'sbc.upc.com', 'addr': '10.241.234.12'},
    'VWMO': {'domain': 'vwmo.nl', 'gateway': 'sbc.vwmo.nl', 'addr': '10.241.234.12'},
    'ZIMO': {'domain': 'ziggo.nl', 'gateway': 'sbc.ziggo.nl', 'addr': '10.241.234.12'}
}

for key in operator:
    zone    = operator[key]['domain']
    domain  = operator[key]['domain']+"."
    gateway = operator[key]['gateway']
    addr    = operator[key]['addr']

    _naptr = '100 10 "S" "SIP+D2T" "" _sip._tcp.{0}'.format(domain)
    _srv   = "10 10 5060 {0}.".format(gateway)

    # create the zone
    payload = {
        "account": "provisioning",
        "soa_edit_api": "INCEPTION-INCREMENT",
        "name": domain,
        "kind": "Native",
        "nameservers": ["ns1.guizy.co.", "ns2.guizy.co."],
	"rrsets": [{
        	"name": domain,
        	"records": [{
            		"content": "ns1.guizy.co. hostmaster.guizy.co. 2016090604 10800 3600 604800 3600",
            		"disabled": False
        	}],
        "ttl": 120,
        "type": "SOA" 
	}]
    }

    #print payload
    print createzone(payload, opts)

    payload = {
        "kind": "Native",
        "name": domain,
        "rrsets": [{
            "name": domain,
            "changetype": "REPLACE",
            "kind": "Native",
            "type": "NAPTR",
            "ttl": 3600,
            "records": [{"content": _naptr, "disabled": False}]
        },
        {
            "name": "_sip._tcp."+domain,
            "changetype": "REPLACE",
            "kind": "Native",
            "type": "SRV",
            "ttl": 3600,
            "records": [{"content": _srv, "disabled": False}]
        },
        {
            "name": gateway+".",
            "changetype": "REPLACE",
            "kind": "Native",
            "type": "A",
            "ttl": 3600,
            "records": [{"content": addr, "disabled": False}]
        }
        ]
    }
    print payload
    print updaterecords(payload, opts, zone)
